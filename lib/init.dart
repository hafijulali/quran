import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:timezone/data/latest_all.dart' as tz_latest;
import 'package:timezone/timezone.dart' as tz;

import 'screens/quran_page/quran_page.dart';
import 'screens/relect_page/reflect_page.dart';
import 'screens/salah_page/salah_page.dart';
import 'screens/settings_page/settings_page.dart';
import 'screens/sunnah_page/sunnah_page.dart';
import 'screens/tarteel_page/tarteel_page.dart';
import 'services/notification/notification.dart';
import 'storage/models/todo_model.dart';

const String appName = 'Islam';
const String settingsDatabaseFileName = 'settings';

late Box<dynamic> settingsDatabase;
late Box<dynamic> secureDatabase;
late PermissionStatus permission;
late String settingsDatabaseFilePath;
late Directory databaseDirectory;
late FlutterLocalNotificationsPlugin notification;
late InAppWebViewController webViewController;
// late InAppWebViewSettings webViewSettings;
// late PullToRefreshController pullToRefreshController;

final keepAlive = InAppWebViewKeepAlive();
WebStorageManager webStorageManager = WebStorageManager.instance();
TextEditingController searchTextController = TextEditingController();
int currentPageIndex = settingsDatabase.get('LandingPage', defaultValue: 0);
String currentPath = '/HomePage';
PageController pageController = PageController(initialPage: currentPageIndex);
bool isDarkMode = settingsDatabase.get('DarkTheme', defaultValue: false);
bool isFullscreenMode = settingsDatabase.get('FullScreen', defaultValue: false);
bool showFloatingButton =
    settingsDatabase.get('FloatingButton', defaultValue: false);
bool useMaterial3 = settingsDatabase.get('Material3', defaultValue: true);
double fontSize = settingsDatabase.get('FontSize', defaultValue: 30.0);

Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  '/Quran': (_) => const Quran(),
  '/Salah': (_) => const Salah(),
  '/Sunnah': (_) => const Sunnah(),
  '/Tarteel': (_) => const Tarteel(),
  '/Reflect': (_) => const Reflect(),
  '/Settings': (_) => const SettingsPage(),
};

const Map<String, Widget> pages = <String, Widget>{
  '/Quran': Quran(),
  '/Salah': Salah(),
  '/Sunnah': Sunnah(),
  '/Tarteel': Tarteel(),
  '/Reflect': Reflect(),
};

const Map<String, String> urls = <String, String>{
  '/Quran': 'https://quran.com',
  '/Salah': 'https://salah.com',
  '/Sunnah': 'https://sunnah.com',
  '/Tarteel': 'https://quranicaudio.com',
  '/Reflect': 'https://quranreflect.com',
};

Future<void> initApp() async {
  await _initServices();
  await _initDatabase();
  await _initPrefs();
}

Future<void> _initServices() async {
  WidgetsFlutterBinding.ensureInitialized();
  Hive
    ..registerAdapter(TodoAdapter())
    ..registerAdapter(TimeOfDayAdapter());

  if (!kIsWeb) {
    tz_latest.initializeTimeZones();
    tz.setLocalLocation(tz.getLocation('Asia/Kolkata'));
  }

  notification = FlutterLocalNotificationsPlugin();
  await notification.initialize(initializationSettings);
}

Future<void> _initDatabase() async {
  await Hive.initFlutter(appName);
  settingsDatabase = await Hive.openBox<dynamic>(settingsDatabaseFileName);
  settingsDatabaseFilePath = settingsDatabase.path.toString();
  if (!kIsWeb) {
    databaseDirectory = Directory(settingsDatabaseFilePath).parent;
  } else {
    (settingsDatabaseFilePath);
  }
}

Future<void> _initPrefs() async {
  isDarkMode =
      await settingsDatabase.get('DarkTheme', defaultValue: isDarkMode);
  currentPath = routes.keys.toList().elementAt(currentPageIndex);
}

ThemeData getTheme() {
  if (isDarkMode) {
    return ThemeData.dark(useMaterial3: useMaterial3);
  }
  return ThemeData.light(useMaterial3: useMaterial3);
}
