import 'package:flutter/material.dart';
import 'package:get/get.dart';

ElevatedButton _signIn(BuildContext context) {
  return ElevatedButton(
    child: const Text('SIGNIN'),
    onPressed: () async {
      try {
        // Cloud.signin();
      } on Exception {
        Get.showSnackbar(
          const GetSnackBar(
            message: 'Error logging in',
            icon: Icon(
              Icons.login_outlined,
              color: Colors.red,
            ),
            duration: Duration(seconds: 3),
          ),
        );
      }
    },
  );
}

ElevatedButton _signOut(BuildContext context) {
  return ElevatedButton(
    child: const Text('SIGNOUT'),
    onPressed: () async {
      try {
        // await GoogleAuth.logout();
        Get.showSnackbar(
          const GetSnackBar(
            message: 'Successfully logged out',
            icon: Icon(
              Icons.logout_outlined,
              color: Colors.green,
            ),
            duration: Duration(seconds: 3),
          ),
        );
      } on Exception {
        Get.showSnackbar(
          const GetSnackBar(
            message: 'Error logging out',
            icon: Icon(
              Icons.logout_outlined,
              color: Colors.red,
            ),
            duration: Duration(seconds: 3),
          ),
        );
      }
    },
  );
}

List<Widget> _authButtons(BuildContext context) {
  return <Widget>[
    _signIn(context),
    const SizedBox(width: 16),
    _signOut(context),
  ];
}

ListTile cloudAuthentication(BuildContext context) {
  return ListTile(
    leading: const Icon(Icons.security_outlined),
    title: const Text('Cloud Authentication'),
    subtitle: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: _authButtons(context),
    ),
    isThreeLine: true,
  );
}
