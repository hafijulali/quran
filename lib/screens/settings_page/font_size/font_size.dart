import 'package:flutter/material.dart';

import '../../../init.dart';

List<DropdownMenuEntry> _dropdownMenuEntries() {
  final List<dynamic> font = List.generate(50, (index) => index += 5);
  final List<DropdownMenuEntry> fontMenuItem = List.empty(growable: true);
  for (final int element in font) {
    fontMenuItem.add(
      DropdownMenuEntry(
        value: element.toDouble(),
        label: element.toString(),
      ),
    );
  }
  return fontMenuItem;
}

ListTile changeFontSize(BuildContext context) {
  return ListTile(
    leading: const Icon(Icons.font_download_outlined),
    title: const Text('Font Size'),
    subtitle: const Text('Font size to use across app'),
    trailing: DropdownMenu<dynamic>(
      hintText: fontSize.toInt().toString(),
      dropdownMenuEntries: _dropdownMenuEntries(),
      onSelected: (value) {
        fontSize = value;
        settingsDatabase.put('FontSize', value);
      },
    ),
  );
}
