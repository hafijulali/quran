import 'package:flutter/material.dart';

import '../../custom/components/webview/webview.dart';
import '../../init.dart';

class Quran extends StatefulWidget {
  const Quran({super.key});

  @override
  _QuranState createState() => _QuranState();
}

class _QuranState extends State<Quran> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(url: urls.values.elementAt(0)),
    );
  }
}
