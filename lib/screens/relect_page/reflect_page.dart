import 'package:flutter/material.dart';
import '../../custom/components/webview/webview.dart';

import '../../init.dart';

class Reflect extends StatefulWidget {
  const Reflect({super.key});

  @override
  _ReflectState createState() => _ReflectState();
}

class _ReflectState extends State<Reflect> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(url: urls.values.elementAt(4)),
    );
  }
}
