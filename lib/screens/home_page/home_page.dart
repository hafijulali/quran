import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../custom/widgets/app_bar.dart';
import '../../custom/widgets/nav_bar.dart';
import '../../init.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Map<int, GlobalKey<NavigatorState>> navigatorKey =
      <int, GlobalKey<NavigatorState>>{
    0: GlobalKey<NavigatorState>(),
    1: GlobalKey<NavigatorState>(),
    2: GlobalKey<NavigatorState>(),
    3: GlobalKey<NavigatorState>(),
    4: GlobalKey<NavigatorState>(),
  };

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  void _onItemTapped(int gotoIndex) {
    setState(() {
      currentPageIndex = gotoIndex;
      currentPath = routes.keys.toList().elementAt(currentPageIndex);
      pageController.jumpToPage(currentPageIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<dynamic>>(
      valueListenable: settingsDatabase.listenable(),
      builder: (BuildContext context, Box<dynamic> storage, _) {
        return Scaffold(
          appBar: isFullscreenMode ? null : appBar(context),
          body: PageView(
            physics: const NeverScrollableScrollPhysics(),
            controller: pageController,
            children: pages.values.toList(),
            onPageChanged: (value) {
              setState(() {
                currentPageIndex = value;
                currentPath = routes.keys.toList().elementAt(currentPageIndex);
              });
            },
          ),
          bottomNavigationBar: isFullscreenMode
              ? null
              : navBar(
                  currentPageIndex,
                  _onItemTapped,
                ),
          floatingActionButton: showFloatingButton
              ? FloatingActionButton.small(
                  onPressed: () =>
                      {setState(() => isFullscreenMode = !isFullscreenMode)},
                  child: isFullscreenMode
                      ? const Icon(Icons.fullscreen_exit_outlined)
                      : const Icon(Icons.fullscreen_outlined),
                )
              : null,
          floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        );
      },
    );
  }

  Navigator navigateTo() => Navigator(
        key: navigatorKey[currentPageIndex],
        onGenerateRoute: (RouteSettings settings) => MaterialPageRoute(
          builder: routes.values.toList().elementAt(currentPageIndex),
        ),
      );
}
