import 'package:flutter/material.dart';
import '../../custom/components/webview/webview.dart';

import '../../init.dart';

class Tarteel extends StatefulWidget {
  const Tarteel({super.key});

  @override
  _TarteelState createState() => _TarteelState();
}

class _TarteelState extends State<Tarteel> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(url: urls.values.elementAt(3)),
    );
  }
}
