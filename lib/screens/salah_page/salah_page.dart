import 'package:flutter/material.dart';
import '../../custom/components/webview/webview.dart';

import '../../init.dart';

class Salah extends StatefulWidget {
  const Salah({super.key});

  @override
  _SalahState createState() => _SalahState();
}

class _SalahState extends State<Salah> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(url: urls.values.elementAt(1)),
    );
  }
}
