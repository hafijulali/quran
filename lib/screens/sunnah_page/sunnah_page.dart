import 'package:flutter/material.dart';
import '../../custom/components/webview/webview.dart';

import '../../init.dart';

class Sunnah extends StatefulWidget {
  const Sunnah({super.key});

  @override
  _SunnahState createState() => _SunnahState();
}

class _SunnahState extends State<Sunnah> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(url: urls.values.elementAt(2)),
    );
  }
}
