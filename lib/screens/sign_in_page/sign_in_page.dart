import 'package:flutter/material.dart';

import '../../custom/widgets/app_bar.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context),
      body: const Center(
        child: ElevatedButton(onPressed: tap, child: Text('SIGNIN')),
      ),
    );
  }
}

Future<void> tap() async {}
