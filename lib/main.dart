import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';

import 'init.dart';
import 'screens/home_page/home_page.dart';

Future<dynamic> main() async {
  await initApp();

  runApp(
    StreamBuilder<BoxEvent>(
      stream: settingsDatabase.watch(),
      builder: (BuildContext context, AsyncSnapshot<BoxEvent> snapshot) {
        return GetMaterialApp(
          home: const HomePage(),
          routes: routes,
          theme: getTheme(),
        );
      },
    ),
  );
}
