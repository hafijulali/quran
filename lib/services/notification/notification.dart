import 'dart:async';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/timezone.dart' as tz;

import '../../init.dart';
import 'platform/android.dart';
import 'platform/ios.dart';
import 'platform/linux.dart';
import 'platform/macos.dart';

final StreamController<ReceivedNotification> didReceiveLocalNotificationStream =
    StreamController<ReceivedNotification>.broadcast();

final InitializationSettings initializationSettings = InitializationSettings(
  android: androidInitializationSettings,
  iOS: iosInitializationSettings,
  macOS: macosInitializationSettings,
  linux: linuxInitializationSettings,
);

class ReceivedNotification {
  ReceivedNotification({
    required this.id,
    required this.title,
    required this.body,
    required this.payload,
  });

  final int id;
  final String? title;
  final String? body;
  final String? payload;
}

Future<void> showNotification(
  int? id,
  String title,
  String? body,
) async {
  await notification.show(
    id ?? 0,
    title,
    body,
    const NotificationDetails(
      android: androidNotificationDetails,
      iOS: iosNotificationDetails,
    ),
    payload: '_none',
  );
}

Future<void> scheduleNotification(int id, String title, String body) async {
  print('time ${tz.TZDateTime.now(tz.local).add(const Duration(seconds: 15))}');

  await notification.zonedSchedule(
    id,
    title,
    body,
    tz.TZDateTime.now(tz.local).add(const Duration(seconds: 15)),
    const NotificationDetails(android: androidNotificationDetails),
    androidScheduleMode: AndroidScheduleMode.inexact,
    uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
  );
}
