import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../../../init.dart';

const AndroidInitializationSettings androidInitializationSettings =
    AndroidInitializationSettings('app_icon');

const AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails(
  appName,
  '$appName main',
  channelDescription: 'notifications from $appName',
  importance: Importance.max,
  priority: Priority.high,
  ticker: 'ticker',
);
