import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

import '../init.dart';

Future<void> checkAndroidStoragePermissions(BuildContext context) async {
  if (permission.isDenied) {
    Get.showSnackbar(
      const GetSnackBar(
        message: 'Error saving todo',
        icon: Icon(
          Icons.error_outlined,
          color: Colors.red,
        ),
        duration: Duration(seconds: 3),
      ),
    );
  }
}
