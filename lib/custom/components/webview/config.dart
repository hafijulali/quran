import 'dart:io';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import '../../../init.dart';

PullToRefreshController pullToRefreshController = PullToRefreshController(
  onRefresh: () async {
    if (Platform.isAndroid) {
      webViewController.reload();
    } else if (Platform.isIOS) {
      webViewController.loadUrl(
          urlRequest: URLRequest(url: await webViewController.getUrl()));
    }
  },
);
InAppWebViewSettings webViewSettings = InAppWebViewSettings(
  mediaPlaybackRequiresUserGesture: false,
  useHybridComposition: true,
  allowsInlineMediaPlayback: true,
  allowBackgroundAudioPlaying: true,
);
