import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import '../../../init.dart';
import '../../widgets/alert_dialog.dart';
import 'config.dart';

class WebView extends StatefulWidget {
  const WebView({super.key, required this.url});
  final String url;

  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {
  final GlobalKey webViewKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: InAppWebView(
            key: webViewKey,
            initialSettings: webViewSettings,
            initialUrlRequest: URLRequest(url: WebUri(widget.url)),
            pullToRefreshController: pullToRefreshController,
            onWebViewCreated: (controller) {
              webViewController = controller;
            },
            onLoadStart: (controller, url) async {
              if (url.toString().contains('/login')) {
                String? inputUrl = await showAlertDialog(
                    context, 'Paste URL Here',
                    withInput: true);
                if (inputUrl!.contains('auth.quran')) {
                  setState(() {
                    webViewController.loadUrl(
                        urlRequest: URLRequest(url: WebUri(inputUrl)));
                  });
                }
              }
            },
            onLoadStop: (controller, url) =>
                pullToRefreshController.endRefreshing(),
            onScrollChanged: (controller, x, y) => {
                  if (y == 0)
                    {setState(() => showFloatingButton = true)}
                  else
                    {setState(() => showFloatingButton = false)},
                  settingsDatabase.put('FloatingButton', showFloatingButton)
                }),
      ),
    );
  }
}
