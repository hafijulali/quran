import 'package:flutter/material.dart';
import '../../init.dart';
import '../navigation/navigate.dart';

Future<String?> showAlertDialog(BuildContext context, String title,
    {String message = 'Enter here',
    bool withInput = false,
    void Function()? onEditingComplete}) async {
  return showDialog<String>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: withInput
            ? Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.blueGrey),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: TextField(
                    textAlign: TextAlign.center,
                    onEditingComplete: onEditingComplete,
                    controller: searchTextController,
                    decoration: InputDecoration(
                      hintText: title,
                      border: InputBorder.none,
                    ),
                  ),
                ),
              )
            : Text(title),
        content: withInput ? null : Text(message),
        actions: <Widget>[
          TextButton(
            onPressed: () => safePopWithResult(context, 'CANCEL'),
            child: const Text('CANCEL'),
          ),
          TextButton(
            onPressed: () {
              withInput
                  ? safePopWithResult(context, searchTextController.text)
                  : safePopWithResult(context, 'OK');
            },
            child: const Text('OK'),
          ),
        ],
      );
    },
  );
}
