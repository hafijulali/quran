import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../services/notification/notification.dart';

import '../../../init.dart';
import '../navigation/navigate.dart';
import 'search_bar.dart';

List<IconButton> _actions(BuildContext context) {
  return <IconButton>[
    IconButton(
      onPressed: () async => safePush('/Settings'),
      icon: const Icon(Icons.settings_outlined),
    ),
  ];
}

AppBar appBar(BuildContext context) {
  return AppBar(
    title: Center(
        child: searchBar(context, () {
      Get.showSnackbar(
        GetSnackBar(
          message: searchTextController.text,
          icon: const Icon(
            Icons.error_outlined,
            color: Colors.red,
          ),
          duration: const Duration(seconds: 3),
        ),
      );
    }, searchTextController, currentPath.substring(1))),
    leading: IconButton(
      icon: const Icon(Icons.arrow_back_outlined),
      onPressed: () {
        if (webViewController.canGoBack() == true) {
          webViewController.goBack();
        } else {
          safePop(context);
        }
        scheduleNotification(0, 'title', 'test body');
      },
    ),
    actions: _actions(context),
  );
}
