import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../init.dart';

List<NavigationDestination> navBarsItems() {
  return <NavigationDestination>[
    NavigationDestination(
      icon: const FaIcon(
        FontAwesomeIcons.bookQuran,
      ),
      label: pages.keys.toList().elementAt(0).substring(1),
    ),
    NavigationDestination(
      icon: const FaIcon(
        FontAwesomeIcons.personPraying,
      ),
      label: pages.keys.toList().elementAt(1).substring(1),
    ),
    NavigationDestination(
      icon: const FaIcon(
        FontAwesomeIcons.person,
      ),
      label: pages.keys.toList().elementAt(2).substring(1),
    ),
    NavigationDestination(
      icon: const Icon(
        Icons.volume_up_rounded,
      ),
      label: pages.keys.toList().elementAt(3).substring(1),
    ),
    NavigationDestination(
      icon: const FaIcon(
        FontAwesomeIcons.mobileScreen,
      ),
      label: pages.keys.toList().elementAt(4).substring(1),
    ),
  ];
}

NavigationBar navBar(int? currentIndex, void Function(int) onItemTapped) {
  return NavigationBar(
    labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
    destinations: navBarsItems(),
    selectedIndex: currentIndex!,
    onDestinationSelected: onItemTapped,
  );
}
