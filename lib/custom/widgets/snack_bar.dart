import 'package:flutter/material.dart';

Future<void> showSnackbar(BuildContext context, String text) async {
  final SnackBar snackBar = SnackBar(
    margin: const EdgeInsets.all(20),
    behavior: SnackBarBehavior.floating,
    content: Text(text),
    action: SnackBarAction(
      label: 'UNDO',
      onPressed: () => {},
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
