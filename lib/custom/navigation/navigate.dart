import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future<void> safePop(BuildContext context) async {
  if (Navigator.canPop(context)) {
    Navigator.pop(context);
  }
}

Future<String?> safePopWithResult(BuildContext context, String result) async {
  if (Navigator.canPop(context)) {
    Navigator.pop(context, result);
  }
  return 'NONE';
}

void safePush(String route) {
  if (Get.currentRoute != route) {
    Get.toNamed<dynamic>(route);
  }
}
