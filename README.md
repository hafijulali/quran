# Islam

**All in one app to do 5 things**
- Quran
- Salah
- Sunnah
- Tarteel
- Reflect

## About
Islam App is a free and opensource app to get islamic information right on demand. One single app for 5 different needs.

## Features
+ ### Cross Platform
&emsp; &emsp; &emsp; Islam App has multiple clients covering all major platforms like Android, iOS, Windows, etc.

+ ### Internet Based
&emsp; &emsp; &emsp; Islam App requires internet, so that latest information is always available to users.

+ ### Material UI
&emsp; &emsp; &emsp; Islam App is built on top of Google's Flutter UI Toolkit that has inbuilt support for Maetrial UI, which feels modern.


## Getting Started

The application can be downloaded and tested for various platforms using the links given below

### Windows
Download the Windows application from this link

### Android
Download the Android application from this link

### iOS
Download the iOS application from this link

### MacOS
Download the MacOS application from this link

### Linux
Download the Linux application from this link

### Web App
Web App is accessible at
<br/> &emsp; - https://hafijulali.gitlab.io/islam/#/ [latest version built directly from master brach]
<br/> &emsp; - https://apps.hafijul.dev/islam


## Building from source
### Android
  - Download and install flutter and related build tools like gradle and jdk.
  - Go to the project root by cd [project].
  - `flutter build apk --release`  for building apk.
### iOS
  - Download and install flutter and related build tools for iOS.
  - Go to the project root by cd [project].
  - `flutter build ios --release`  for building ios app.
### Linux
  - Download and install flutter and related build tools like cmake and ninja.
  - Go to the project root by cd [project].
  - `flutter build linux --release`  for building linux executable bundle.
### MacOS
  - Download and install flutter and related build tools for MacOS.
  - Go to the project root by cd [project].
  - `flutter build macos --release`  for building macos executable bundle.
### Windows
  - Download and install flutter and related build tools like cmake and windows-sdk.
  - Go to the project root by cd [project].
  - `flutter build windows --release`  for building windows executable bundle.
### Web
  - Download and install flutter and related build tools for Web.
  - Go to the project root by cd [project].
  - `flutter build web --release`  for building javascript app

## For developers





## License
This project is licensed under [Apache License2.0](https://apache.org/licenses/LICENSE-2.0).


## Contributing
Project contribution can be made  by forking the repository and making a pull request. The merge code will be thoroughly tested and then merged to respective branch(except master). 

## Project Status
The project is being actively developed by the author(s) depending upon their availability. We as community can make opensource software accessible and reliable for all.


